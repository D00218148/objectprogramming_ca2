package PetRegistrySystem;
//Author: Antanas Zalisauskas(D00218148)
//Code for current date was made with the help from https://www.javatpoint.com/java-get-current-date

import java.io.*;
import java.util.*;
import java.time.format.DateTimeFormatter;
import java.time.LocalDateTime;

public class Main
{
    private static ArrayList<Owner> owners = new ArrayList<Owner>();
    private static ArrayList<Mammal> mammals = new ArrayList<Mammal>();
    private static ArrayList<Fish> fish = new ArrayList<Fish>();
    private static ArrayList<Bird> birds = new ArrayList<Bird>();
    private static Scanner sc = new Scanner(System.in);
    private static DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd");

    private enum MenuOptions
    {
        SHOWOPTIONS,
        ADDOWNER,
        ADDPET,
        SHOWPETS,
        EDITPET,
        REMOVEPET,
        SHOWOWNERS,
        EDITOWNER,
        REMOVEOWNER,
        ASSIGNPET,
        ORDER,
        OWNERPETS,
        CATEGORY,
        STATISTICS,
        QUIT
    }


    public static void main(String[] args)
    {
        ReadInPetsFromFile();
        ReadInOwnersFromFile();
        MenuOptions[] options = MenuOptions.values();
        boolean quit = false;

        System.out.println("Welcome to Dundalk Pet Registry. Please enter your choice");
        OutputAvailableOptions();

        try
        {
            while(!quit)
            {
                int choice = choiceInputInt();
                switch(options[choice - 1])
                //switch(choiceInputString().toUpperCase())
                {
                    case SHOWOPTIONS:
                        OutputAvailableOptions();
                        break;
                    case ADDOWNER:
                        AddNewOwner();
                        break;
                    case ADDPET:
                        AddNewPet();
                        break;
                    case SHOWPETS:
                        ShowExistingPets();
                        break;
                    case EDITPET:
                        EditPets();
                        break;
                    case REMOVEPET:
                        RemovePet();
                        break;
                    case SHOWOWNERS:
                        ShowExistingOwners();
                        break;
                    case EDITOWNER:
                        EditOwners();
                        break;
                    case REMOVEOWNER:
                        RemoveOwner();
                        break;
                    case ASSIGNPET:
                        AssignPetToOwner();
                        break;
                    case ORDER:
                        OrderPets();
                        break;
                    case OWNERPETS:
                        PrintPetsFromOwner();
                        break;
                    case CATEGORY:
                        PetsCategory();
                        break;
                    case STATISTICS:
                        PetAgeAvg();
                        PetPercent();
                        OutputAvailableOptions();
                        break;
                    case QUIT:
                        quit = true;
                        break;
                }
            }
        }
        catch(NoSuchElementException e)
        {
            System.out.println(e.getMessage());
        }
        catch(IndexOutOfBoundsException e)
        {
            System.out.println("Value entered is invalid or outside option range");
        }
    }

    private static void OutputAvailableOptions()
    {
        System.out.println("Enter 1 to show available options");
        System.out.println("Enter 2 to add a new owner");
        System.out.println("Enter 3 to add a new pet");
        System.out.println("Enter 4 to show existing pets");
        System.out.println("Enter 5 to edit pet details");
        System.out.println("Enter 6 to remove a pet");
        System.out.println("Enter 7 to show existing owners");
        System.out.println("Enter 8 to edit owner details");
        System.out.println("Enter 9 to remove an owner");
        System.out.println("Enter 10 to assign a pet to an owner");
        System.out.println("Enter 11 to show ordered list of pets");
        System.out.println("Enter 12 to see pets assigned to an owner");
        System.out.println("Enter 13 to show pets of specific type");
        System.out.println("Enter 14 to show statistics for pets");
        System.out.println("Enter 15 to quit");
    }

    /*
     * choiceInputInt, choiceInputString, choiceInputDouble are used whenever user input
     * is required, is user input does not match required parameter, a print out is shown,
     * the buffer is cleaned and the method runs again. The method doesn't end until the
     * input matches the parameter which is then returned
     */
    private static int choiceInputInt()
    {
        int choice = 0;
        try
        {
            choice = sc.nextInt();
            sc.nextLine();
        }
        catch(InputMismatchException e)
        {
            System.out.println("Invalid input. Please enter a whole number");
            sc.nextLine();
            choiceInputInt();
        }
        catch(NoSuchElementException e)
        {
            throw new NoSuchElementException("Please don't enter CTRL+D as this input closes System.in meaning nothing else can be done with the program");
        }
        return choice;
    }

    private static double choiceInputDouble()
    {
        double choice = 0;
        try
        {
            choice = sc.nextDouble();
            sc.nextLine();
        }
        catch(InputMismatchException e)
        {
            System.out.println("Invalid input. Please enter a whole number");
            sc.nextLine();
            choiceInputDouble();
        }
        catch(NoSuchElementException e)
        {
            throw new NoSuchElementException("Please don't enter CTRL+D as this input closes System.in meaning nothing else can be done with the program");
        }
        return choice;
    }

    private static String choiceInputString()
    {
        String choice = null;
        try
        {
            choice = sc.nextLine();
        }
        catch(InputMismatchException e)
        {
            System.out.println("Invalid input. Please enter a valid word");
            sc.nextLine();
            choiceInputString();
        }
        catch(NoSuchElementException e)
        {
            throw new NoSuchElementException("Please don't enter CTRL+D as this input closes System.in meaning nothing else can be done with the program");
        }
        return choice;
    }

    /*
     * AddNewOwner is called whenever the user wants to add a new owner.
     * If owner arrayList size == 0, the owner is added, else the owner
     * details are checked against other owners in the arrayList and only
     * added if there is no other owner with same details. Once owner is
     * added, the details are saved onto a text file
     */
    private static void AddNewOwner()
    {
        String name;
        String email;
        String telephone;
        String address;
        boolean ownerExists = false;

        System.out.println("Enter owner name");
        name = choiceInputString();
        System.out.println("Enter owner email");
        email = choiceInputString();
        System.out.println("Enter owner telephone");
        telephone = choiceInputString();
        System.out.println("Enter owner address");
        address = choiceInputString();

        Owner temp = new Owner(name ,email, telephone, address, GenerateOwnerID());
        if(owners.size() == 0)
        {
            owners.add(temp);
            WriteOwnerDetailsToFile();
            System.out.println("Owner added");
        }
        else
        {
            System.out.println("Checking if owner exists");
            for(Owner owner : owners)
            {
                ownerExists = owner.compareOwners(temp);
                if(ownerExists)
                {
                    System.out.println("Owner exists");
                    break;
                }
            }
            if(!ownerExists)
            {
                owners.add(temp);
                WriteOwnerDetailsToFile();
                System.out.println("Owner added");
            }
        }
        OutputAvailableOptions();
    }

    /*
     * EditOwners is used whenever the user wants to edit an owners details
     * if owner arraylist size is not 0, the method continues, else a
     * print out is shown. User is required to input owner id to edit
     * corresponding owner. If id exists, user inputs what they would like to
     * edit about owner and then enter the new value. Once edited, The details
     * are saved onto a text file
     */
    private static void EditOwners()
    {
        if(owners.size() != 0)
        {
            PrintAllOwners();
            System.out.println("Enter owner ID to edit corresponding owner");
            String ID = choiceInputString();
            int index = CheckOwnerID(owners, ID);

            if(index >= 0)
            {
                System.out.println("Please choose what to edit(name, email, telephone, address)");
                String input = choiceInputString();
                switch(input)
                {
                    case "name":
                        System.out.println("Enter new name");
                        String name = choiceInputString();
                        owners.get(index).setName(name);
                        break;
                    case "email":
                        System.out.println("Enter new email");
                        String email = choiceInputString();
                        owners.get(index).setEmail(email);
                        break;
                    case "telephone":
                        System.out.println("Enter new telephone");
                        String telephone = choiceInputString();
                        owners.get(index).setTelephone(telephone);
                        break;
                    case "address":
                        System.out.println("Enter new address");
                        String address = choiceInputString();
                        owners.get(index).setAddress(address);
                        break;
                }
                WriteOwnerDetailsToFile();
                System.out.println("Owner details updated");
            }
            else
            {
                System.out.println("Owner ID not found");
            }
        }
        else
        {
            System.out.println("There are currently no owners registered");
        }
        OutputAvailableOptions();
    }

    /*
     * RemoveOwner works similar to EditOwner but instead of the user editing
     * details of the owner, the owner with id that user inputted gets removed#
     * from arrayList
     */
    private static void RemoveOwner()
    {
        if(owners.size() != 0)
        {
            PrintAllOwners();
            System.out.println("Enter owner ID to remove corresponding owner");
            String ID = choiceInputString();
            int index = CheckOwnerID(owners, ID);

            if(index >= 0)
            {
                owners.remove(index);
                WriteOwnerDetailsToFile();
                System.out.println("Owner details updated");
            }
            else
            {
                System.out.println("Owner ID not found");
            }
        }
        else
        {
            System.out.println("There are currently no owners registered");
        }
        OutputAvailableOptions();
    }

    /*
     * ShowExistingOwners reads a text file which holds owner details and prints them out
     */
    private static void ShowExistingOwners()
    {
        try(Scanner scanner = new Scanner(new BufferedReader(new FileReader("ownerDetails.txt"))))
        {
            while(scanner.hasNextLine())
            {
                String input = scanner.nextLine();
                String[] data = input.split(",");
                String ID = data[0];
                String name = data[1];
                String email = data[2];
                String address = data[3];
                String telephone = data[4];

                System.out.println(ID + ": " + name + " " + email + " " + address + " " + telephone);
            }
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
        OutputAvailableOptions();
    }

    /*
     * PrintAllOwners calls a toString method in Owner for each owner in arrayList
     */
    private static void PrintAllOwners()
    {
        for(Owner owner : owners)
        {
            System.out.println(owner.toString());
        }
    }

    /*
     * CheckOwnerID calls a method in Owner which checks if id exists.
     * If true, the method returns i which is used as the index to find the owner
     * @param id - user inputted, checked against existing owners
     */
    private static int CheckOwnerID(ArrayList<Owner> owners, String id)
    {
        for(int i = 0; i < owners.size(); i++)
        {
            if(owners.get(i).checkID(id))
            {
                return i;
            }
        }
        return -1;
    }

    /*
     * GenerateOwnerID generates an id for an owner from 1-1000
     */
    private static String GenerateOwnerID()
    {
        int random = (int)(Math.random()*1000);

        return "OID" + random;
    }

    private static void WriteOwnerDetailsToFile()
    {
        try(BufferedWriter ownerDetails = new BufferedWriter(new FileWriter("ownerDetails.txt")))
        {
            if(owners.size() != 0)
            {
                for(Owner owner: owners)
                {
                    ownerDetails.write(owner.getOwnerID() + "," + owner.getName() + "," + owner.getEmail() + "," + owner.getAddress() + "," + owner.getTelephone() + "\n");
                }
            }
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
    }

    /*
     * AddNewPet requires user to input either mammal, bird or fish animal type
     * and calls method to enter the rest of the pet details.
     * If input does not equal to mammal, bird or fish, the method runs again.
     */
    private static void AddNewPet()
    {
        System.out.println("Please enter type of animal(mammal, bird, fish)");
        String animalType = choiceInputString();

        if(animalType.toLowerCase().equals("mammal"))
        {
            EnterPetDetails(animalType);
        }
        else if(animalType.toLowerCase().equals("bird"))
        {
            EnterPetDetails(animalType);
        }
        else if(animalType.toLowerCase().equals("fish"))
        {
            EnterPetDetails(animalType);
        }
        else
        {
            System.out.println("Unknown animal type " + animalType);
            AddNewPet();
        }
    }

    /*
     * EnterPetDetails works similar to AddNewOwner where user inputs
     * specific details about the pet. Additional options appear based
     * on the animalType. Details are checked to see if its a duplicate.#
     *
     * @param animalType - this can either be 'mammal', 'bird' or 'fish'
     * and unlocks additional options based on what the user entered in
     * AddNewPet
     */
    private static void EnterPetDetails(String animalType)
    {
        String type;
        String name;
        String breed;
        int age;
        String colour;
        String gender;
        boolean neutered;
        boolean petExists = false;
        double wingspan;
        boolean canFly;
        String waterType;

        System.out.println("Enter type of pet");
        type = choiceInputString();
        System.out.println("Enter name");
        name = choiceInputString();
        System.out.println("Enter breed");
        breed = choiceInputString();
        System.out.println("Enter age");
        age = choiceInputInt();
        System.out.println("Enter colour");
        colour = choiceInputString();
        System.out.println("Enter gender");
        gender = choiceInputString();
        if(animalType.equalsIgnoreCase("mammal"))
        {
            neutered = MammalAndBirdAdditionalOptions(animalType.toLowerCase());
            Mammal temp = new Mammal(dtf.format(RegisterDate()), GeneratePetID(),"", animalType,type, name, breed, age, colour, gender, neutered);
            if(mammals.size() == 0)
            {
                mammals.add(temp);
                WritePetDetailsToFile();
                System.out.println("Pet has been added");
            }
            else
            {
                System.out.println("Checking if pet exists");
                for(int i = 0; i < mammals.size(); i++)
                {
                    petExists = mammals.get(i).comparePets(temp);
                    if(petExists)
                    {
                        System.out.println("Pet already exists");
                        break;
                    }
                }
                if(!petExists)
                {
                    mammals.add(temp);
                    WritePetDetailsToFile();
                    System.out.println("Pet has been added");
                }
            }
        }
        else if(animalType.equalsIgnoreCase("bird"))
        {
            System.out.println("Enter wingspan");
            wingspan = choiceInputDouble();
            canFly = MammalAndBirdAdditionalOptions(animalType.toLowerCase());
            Bird temp = new Bird(dtf.format(RegisterDate()), GeneratePetID(),"", animalType,type, name, breed, age, colour, gender, wingspan, canFly);
            if(birds.size() == 0)
            {
                birds.add(temp);
                WritePetDetailsToFile();
                System.out.println("Pet has been added");
            }
            else
            {
                System.out.println("Checking if pet exists");
                for(int i = 0; i < birds.size(); i++)
                {
                    petExists = birds.get(i).comparePets(temp);
                    if(petExists)
                    {
                        System.out.println("Pet already exists");
                        break;
                    }
                }
                if(!petExists)
                {
                    birds.add(temp);
                    WritePetDetailsToFile();
                    System.out.println("Pet has been added");
                }
            }
        }
        else
        {
            System.out.println("Enter water type");
            waterType = choiceInputString();
            Fish temp = new Fish(dtf.format(RegisterDate()), GeneratePetID(),"", animalType,type, name, breed, age, colour, gender, waterType);
            if(fish.size() == 0)
            {
                fish.add(temp);
                WritePetDetailsToFile();
                System.out.println("Pet has been added");
            }
            else
            {
                System.out.println("Checking if pet exists");
                for(int i = 0; i < fish.size(); i++)
                {
                    petExists = fish.get(i).comparePets(temp);
                    if(petExists)
                    {
                        System.out.println("Pet already exists");
                        break;
                    }
                }
                if(!petExists)
                {
                    fish.add(temp);
                    WritePetDetailsToFile();
                    System.out.println("Pet has been added");
                }
            }
        }
        OutputAvailableOptions();
    }

    /*
     * RegisterDate generates and returns the date the pet was added
     */
    private static LocalDateTime RegisterDate()
    {
        return LocalDateTime.now();
    }

    /*
     * MammalAndBirdAdditionalOptions has the user enter yes or no for
     * neutered or canFly based on animalType and sets neutered or canFly
     * to true or false and returns neutered or canFly
     *
     * @param animalType - if animalType is 'mammal' then the method returns neutered
     * else it returns canFly
     */
    private static boolean MammalAndBirdAdditionalOptions(String animalType)
    {
        if(animalType.equals("mammal"))
        {
            boolean neutered;
            System.out.println("Is pet neutered? Y/N");
            String choice = choiceInputString();
            if(choice.toLowerCase().equals("y") || choice.toLowerCase().equals("yes"))
            {
                neutered = true;
            }
            else if(choice.toLowerCase().equals("n") || choice.toLowerCase().equals("no"))
            {
                neutered = false;
            }
            else
            {
                System.out.println("Invalid input. Neutered has been set to NO");
                neutered = false;
            }
            return neutered;
        }
        else
        {
            boolean canFly;
            System.out.println("Can bird fly? Y/N");
            String choice = choiceInputString();
            if(choice.toLowerCase().equals("y") || choice.toLowerCase().equals("yes"))
            {
                canFly = true;
            }
            else if(choice.toLowerCase().equals("n") || choice.toLowerCase().equals("no"))
            {
                canFly = false;
            }
            else
            {
                System.out.println("Invalid input. Can bird fly has been set to NO");
                canFly = false;
            }
            return canFly;
        }
    }

    /*
     * GeneratePetID works the same way as GenerateOwnerID but returns a different string
     */
    private static String GeneratePetID()
    {
        int random = (int)(Math.random()*1000);

        return "PID" + random;
    }

    /*
     * WritePetDetailsToFile is called whenever a pet is removed, updated or added
     * This method writes pet details onto a text file
     */
    private static void WritePetDetailsToFile()
    {
        try(BufferedWriter petDetails = new BufferedWriter(new FileWriter("petDetails.txt")))
        {
            if(mammals.size() != 0)
            {
                for(Mammal mammal: mammals)
                {
                    petDetails.write(mammal.getDateRegistered() + "," + mammal.getPetID() + "," + mammal.getOwnerID() + "," + mammal.getAnimalType() + "," + mammal.getType() + "," + mammal.getName() + "," +
                            mammal.getBreed() + "," + mammal.getAge() + "," + mammal.getColour() + "," +
                            mammal.getGender());
                    if(mammal.isNeutered())
                    {
                        petDetails.write(",true");
                    }
                    else
                    {
                        petDetails.write(",false");
                    }
                    petDetails.write("\n");
                }
            }
            if(birds.size() != 0)
            {
                for(Bird bird: birds)
                {
                    petDetails.write(bird.getDateRegistered() + "," + bird.getPetID() + "," + bird.getOwnerID() + "," + bird.getAnimalType() + "," + bird.getType() + "," + bird.getName() + "," +
                            bird.getBreed() + "," + bird.getAge() + "," + bird.getColour() + "," +
                            bird.getGender() + "," + bird.getWingspan());
                    if(bird.CanFly())
                    {
                        petDetails.write(",true");
                    }
                    else
                    {
                        petDetails.write(",false");
                    }
                    petDetails.write("\n");
                }
            }
            if(fish.size() != 0)
            {
                for(Fish petFish: fish)
                {
                    petDetails.write(petFish.getDateRegistered() + "," + petFish.getPetID() + "," + petFish.getOwnerID() + "," + petFish.getAnimalType() + "," + petFish.getType() + "," + petFish.getName() + "," +
                            petFish.getBreed() + "," + petFish.getAge() + "," + petFish.getColour() + "," +
                            petFish.getGender() + "," + petFish.getWaterType() + "\n");
                }
            }

        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
    }

    /*
     * ShowExistingPets reads and outputs details from a text file
     */
    private static void ShowExistingPets()
    {
        try(Scanner scanner = new Scanner(new BufferedReader(new FileReader("petDetails.txt"))))
        {
            while(scanner.hasNextLine())
            {
                String input = scanner.nextLine();
                String[] data = input.split(",");
                String dateRegistered = data[0];
                String petID = data[1];
                String ownerID = data[2];
                String animalType = data[3];
                String type = data[4];
                String name = data[5];
                String breed = data[6];
                String age = data[7];
                String colour = data[8];
                String gender = data[9];
                if(animalType.equals("mammal"))
                {
                    String neutered = data[10];
                    System.out.println(dateRegistered + " " + petID + ": " + ownerID + " " + animalType + " " + type + " " + name + " " + breed + " " + age + " " + colour + " " + gender + " " + neutered);
                }
                else if(animalType.equals("bird"))
                {
                    String wingspan = data[10];
                    String canFly = data[11];
                    System.out.println(dateRegistered + " " + petID + ": " + ownerID + " " + animalType + " " + type + " " + name + " " + breed + " " + age + " " + colour + " " + gender + " " + wingspan + " " + canFly);
                }
                else
                {
                    String waterType = data[10];
                    System.out.println(dateRegistered + " " + petID + ": " + ownerID + " " + animalType + " " + type + " " + name + " " + breed + " " + age + " " + colour + " " + gender + " " + waterType);
                }
            }
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
        OutputAvailableOptions();
    }

    /*
     * EditPets has the user input animal type they want to edit
     * and calls corresponding method to edit pet
     */
    private static void EditPets()
    {
        System.out.println("Enter animal type(mammal, bird, fish)");
        String input = choiceInputString();
        switch(input.toLowerCase())
        {
            case "mammal":
                EditMammal();
                break;
            case "bird":
                EditBird();
                break;
            case "fish":
                EditFish();
                break;
        }
    }

    /*
     * EditMammal, EditBird, EditFish have the user input id they want to edit
     * if id is found, the user enters what they want to edit and enters new value
     * edited details are saved onto text file
     */
    private static void EditMammal()
    {
        PrintMammals();
        System.out.println("Enter pet ID of pet to be changed");
        String input = choiceInputString();
        int index = checkIDMammal(input);
        if(index >= 0)
        {
            System.out.println("Enter category to be changed(type, name, breed, age, colour, gender, neutered)");
            String category = choiceInputString();
            switch(category.toLowerCase())
            {
                case "type":
                    System.out.println("Enter new type of pet");
                    String type = choiceInputString();
                    mammals.get(index).setType(type);
                    break;
                case "name":
                    System.out.println("Enter new name of pet");
                    String name = choiceInputString();
                    mammals.get(index).setName(name);
                    break;
                case "breed":
                    System.out.println("Enter new breed of pet");
                    String breed = choiceInputString();
                    mammals.get(index).setBreed(breed);
                    break;
                case "age":
                    System.out.println("Enter new age of pet");
                    int age = choiceInputInt();
                    mammals.get(index).setAge(age);
                    break;
                case "colour":
                    System.out.println("Enter new colour of pet");
                    String colour = choiceInputString();
                    mammals.get(index).setColour(colour);
                    break;
                case "gender":
                    System.out.println("Enter new gender of pet");
                    String gender = choiceInputString();
                    mammals.get(index).setGender(gender);
                    break;
                case "neutered":
                    boolean neutered = MammalAndBirdAdditionalOptions(mammals.get(index).getAnimalType());
                    mammals.get(index).setNeutered(neutered);
                    break;
            }
            WritePetDetailsToFile();
            System.out.println("Details updated");
            OutputAvailableOptions();
        }
        else
        {
            System.out.println("Pet ID not found");
            OutputAvailableOptions();
        }
    }

    private static void EditBird()
    {
        PrintBirds();
        System.out.println("Enter pet ID of pet to be changed");
        String input = choiceInputString();
        int index = checkIDBird(input);
        if(index >= 0)
        {
            System.out.println("Enter category to be changed(type, name, breed, age, colour, gender, wingspan, can fly)");
            String category = choiceInputString();
            switch(category.toLowerCase())
            {
                case "type":
                    System.out.println("Enter new type of pet");
                    String type = choiceInputString();
                    birds.get(index).setType(type);
                    break;
                case "name":
                    System.out.println("Enter new name of pet");
                    String name = choiceInputString();
                    birds.get(index).setName(name);
                    break;
                case "breed":
                    System.out.println("Enter new breed of pet");
                    String breed = choiceInputString();
                    birds.get(index).setBreed(breed);
                    break;
                case "age":
                    System.out.println("Enter new age of pet");
                    int age = choiceInputInt();
                    birds.get(index).setAge(age);
                    break;
                case "colour":
                    System.out.println("Enter new colour of pet");
                    String colour = choiceInputString();
                    birds.get(index).setColour(colour);
                    break;
                case "gender":
                    System.out.println("Enter new gender of pet");
                    String gender = choiceInputString();
                    birds.get(index).setGender(gender);
                    break;
                case "can fly":
                    boolean canFly = MammalAndBirdAdditionalOptions(birds.get(index).getAnimalType());
                    birds.get(index).setCanFly(canFly);
                    break;
                case "wingspan":
                    System.out.println("Enter new wingspan");
                    double wingspan = choiceInputDouble();
                    birds.get(index).setWingspan(wingspan);
                    break;
            }
            WritePetDetailsToFile();
            System.out.println("Details updated");
            OutputAvailableOptions();
        }
        else
        {
            System.out.println("Pet ID not found");
            OutputAvailableOptions();
        }
    }

    private static void EditFish()
    {
        PrintFish();
        System.out.println("Enter pet ID of pet to be changed");
        String input = choiceInputString();
        int index = checkIDFish(input);
        if(index >= 0)
        {
            System.out.println("Enter category to be changed(type, name, breed, age, colour, gender, water type)");
            String category = choiceInputString();
            switch(category.toLowerCase())
            {
                case "type":
                    System.out.println("Enter new type of pet");
                    String type = choiceInputString();
                    fish.get(index).setType(type);
                    break;
                case "name":
                    System.out.println("Enter new name of pet");
                    String name = choiceInputString();
                    fish.get(index).setName(name);
                    break;
                case "breed":
                    System.out.println("Enter new breed of pet");
                    String breed = choiceInputString();
                    fish.get(index).setBreed(breed);
                    break;
                case "age":
                    System.out.println("Enter new age of pet");
                    int age = choiceInputInt();
                    fish.get(index).setAge(age);
                    break;
                case "colour":
                    System.out.println("Enter new colour of pet");
                    String colour = choiceInputString();
                    fish.get(index).setColour(colour);
                    break;
                case "gender":
                    System.out.println("Enter new gender of pet");
                    String gender = choiceInputString();
                    fish.get(index).setGender(gender);
                    break;
                case "water type":
                    System.out.println("Enter new water type of pet");
                    String waterType = choiceInputString();
                    fish.get(index).setWaterType(waterType);
                    break;
            }
            WritePetDetailsToFile();
            System.out.println("Details updated");
            OutputAvailableOptions();
        }
        else
        {
            System.out.println("Pet ID not found");
            OutputAvailableOptions();
        }
    }

    /*
     * checkIDMammal, checkIDBird, checkIDFish are called whenever the
     * user wants to edit or remove a pet. Method returns i for index if
     * id is found else it returns -1
     *
     * @param input - user inputted id which is checked against existing pets
     */
    private static int checkIDMammal(String input)
    {
        for(int i = 0; i < mammals.size(); i++)
        {
            if(mammals.get(i).checkID(input))
            {
                return i;
            }
        }
        return -1;
    }

    private static int checkIDBird(String input)
    {
        for(int i = 0; i < birds.size(); i++)
        {
            if(birds.get(i).checkID(input))
            {
                return i;
            }
        }
        return -1;
    }

    private static int checkIDFish(String input)
    {
        for(int i = 0; i < fish.size(); i++)
        {
            if(fish.get(i).checkID(input))
            {
                return i;
            }
        }
        return -1;
    }

    /*
     * RemovePet works similar to EditMammal, EditBird and EditFish
     * but instead removes pet from a specific animal type once the
     * pet id is found
     */
    private static void RemovePet()
    {
        if(mammals.size() > 0 || birds.size() > 0 || fish.size() > 0)
        {
            int index;
            System.out.println("Mammals");
            PrintMammals();
            System.out.println("Birds");
            PrintBirds();
            System.out.println("Fish");
            PrintFish();

            System.out.println("Enter animal type(mammal, bird, fish)");
            String animalType = choiceInputString();
            System.out.println("Enter pet ID of pet to be removed");
            String input = choiceInputString();
            switch(animalType.toLowerCase())
            {
                case "mammal":
                    index = checkIDMammal(input);
                    if(index >= 0)
                    {
                        mammals.remove(index);
                    }
                    break;
                case "bird":
                    index = checkIDBird(input);
                    if(index >= 0)
                    {
                        birds.remove(index);
                    }
                    break;
                case "fish":
                    index = checkIDFish(input);
                    if(index >= 0)
                    {
                        fish.remove(index);
                    }
                    break;
            }
            WritePetDetailsToFile();
            System.out.println("Pet removed");
            OutputAvailableOptions();
        }
        else
        {
            System.out.println("No pets have been added");
            OutputAvailableOptions();
        }

    }

    /*
     * PrintMammal, PrintBird, PrintFish calls toString method for each
     * pet in arrayList and outputs them
     */
    private static void PrintMammals()
    {
        for(int i = 0; i < mammals.size(); i++)
        {
            System.out.println(mammals.get(i).toString());
        }
    }

    private static void PrintBirds()
    {
        for(int i = 0; i < birds.size(); i++)
        {
            System.out.println(birds.get(i).toString());
        }
    }

    private static void PrintFish()
    {
        for(int i = 0; i < fish.size(); i++)
        {
            System.out.println(fish.get(i).toString());
        }
    }

    /*
     * AssignPetToOwner allows user to assign an owner to a pet
     * If owner id is found, method asks user to input animal
     * type to be assigned(mammal, bird, fish). Pets
     * from animal type are printed out and user enters ID of pet.
     * Once id is found, a method is called which sets a pets owner
     * id to a new one
     */
    private static void AssignPetToOwner()
    {
        int ownerIndex;
        String petID;
        int petIndex;

        if(owners.size() != 0)
        {
            PrintAllOwners();
            System.out.println("Select owner ID to assign pet to");
            String ownerID = choiceInputString();
            ownerIndex = CheckOwnerID(owners, ownerID);
            if(ownerIndex >= 0)
            {
                PrintMammals();
                PrintBirds();
                PrintFish();
                System.out.println("Enter animal type(mammal, bird, fish)");
                String input = choiceInputString();
                switch(input.toLowerCase())
                {
                    case "mammal":
                        PrintMammals();
                        System.out.println("Enter pet ID to assign to owner ID");
                        petID = choiceInputString();
                        petIndex = checkIDMammal(petID);
                        if(petIndex >= 0)
                        {
                            UpdatePetOwnerID(ownerID, petIndex, input);
                            System.out.println("Pet ID " + petID + " assigned to owner ID " + ownerID);
                        }
                        else
                        {
                            System.out.println("Pet ID not found");
                        }
                        break;
                    case "bird":
                        PrintBirds();
                        System.out.println("Enter pet ID to assign to owner ID");
                        petID = choiceInputString();
                        petIndex = checkIDBird(petID);
                        if(petIndex >= 0)
                        {
                            UpdatePetOwnerID(ownerID, petIndex, input);
                            System.out.println("Pet ID " + petID + " assigned to owner ID " + ownerID);
                        }
                        else
                        {
                            System.out.println("Pet ID not found");
                        }
                        break;
                    case "fish":
                        PrintFish();
                        System.out.println("Enter pet ID to assign to owner ID");
                        petID = choiceInputString();
                        petIndex = checkIDFish(petID);
                        if(petIndex >= 0)
                        {
                            UpdatePetOwnerID(ownerID, petIndex, input);
                            System.out.println("Pet ID " + petID + " assigned to owner ID " + ownerID);
                        }
                        else
                        {
                            System.out.println("Pet ID not found");
                        }
                        break;
                }
                WritePetDetailsToFile();
            }
            else
            {
                System.out.println("Owner ID not found");
            }
        }
        else
        {
            System.out.println("There are no owners to assign a pet to");
        }
        OutputAvailableOptions();
    }

    /*
     * UpdatePetOwnerID is callede whenever a pet is assigned to an owner.
     * the method calls a setter in Mammal, Bird or Fish and updates ownerID
     * for a pet
     *
     * @param id - the owner id the pet will be assigned to
     * @param index - used to find pet in an arraylist
     * @param animalType - used to get corresponding arrayList
     */
    private static void UpdatePetOwnerID(String id, int index, String animalType)
    {
        if(animalType.equals("mammal"))
        {
            mammals.get(index).SetOwnerID(id);
        }
        else if(animalType.equals("bird"))
        {
            birds.get(index).SetOwnerID(id);
        }
        else
        {
            fish.get(index).SetOwnerID(id);
        }
    }

    /*
     * OrderPets asks user what they want to order pet arrayLists by
     * and calls corresponding methods
     */
    private static void OrderPets()
    {
        System.out.println("Order by: PetID, Gender or Age?");
        String input = choiceInputString();

        switch(input.toLowerCase())
        {
            case "petid":
                OrderPetsID();
                break;
            case "gender":
                OrderPetsGender();
                break;
            case "age":
                OrderPetsAge();
                break;
        }
        OutputAvailableOptions();
    }

    private static void OrderPetsID()
    {
        mammals.sort(new PetOrderByID());
        System.out.println("Mammals");
        for(Mammal mammal: mammals)
        {
            System.out.println(mammal.toString());
        }
        birds.sort(new PetOrderByID());
        System.out.println("Birds");
        for(Bird bird: birds)
        {
            System.out.println(bird.toString());
        }
        fish.sort(new PetOrderByID());
        System.out.println("Fish");
        for(Fish petFish: fish)
        {
            System.out.println(petFish.toString());
        }
    }

    private static void OrderPetsGender()
    {
        mammals.sort(Mammal.GenderComparator);
        System.out.println("Mammals");
        for(Mammal mammal: mammals)
        {
            System.out.println(mammal.toString());
        }
        birds.sort(Bird.GenderComparator);
        System.out.println("Birds");
        for(Bird bird: birds)
        {
            System.out.println(bird.toString());
        }
        fish.sort(Fish.GenderComparator);
        System.out.println("Fish");
        for(Fish petFish: fish)
        {
            System.out.println(petFish.toString());
        }
    }

    private static void OrderPetsAge()
    {
        mammals.sort(Mammal.AgeComparator);
        System.out.println("Mammals");
        for(Mammal mammal: mammals)
        {
            System.out.println(mammal.toString());
        }
        birds.sort(Bird.AgeComparator);
        System.out.println("Birds");
        for(Bird bird: birds)
        {
            System.out.println(bird.toString());
        }
        fish.sort(Fish.AgeComparator);
        System.out.println("Fish");
        for(Fish petFish: fish)
        {
            System.out.println(petFish.toString());
        }
    }

    /*
     * PetsCategory asks user what animal type of pet they want to see
     * if they type 'mammal' it calls PrintMammals.
     * If they type 'bird' it calls PrintBirds.
     * If they type 'fish' it calls PrintFish.
     * Else they get a print out saying their input is invalid
     */
    private static void PetsCategory()
    {
        System.out.println("Type 'mammal', 'bird' or 'fish' to show corresponding list of pets");
        String input = choiceInputString();
        if(input.toLowerCase().contains("mammal"))
        {
            PrintMammals();
        }
        else if(input.toLowerCase().contains("bird"))
        {
            PrintBirds();
        }
        else if(input.toLowerCase().contains("fish"))
        {
            PrintFish();
        }
        else
        {
            System.out.println(input + " is an invalid input");
        }
        OutputAvailableOptions();
    }

    /*
     * PrintPetsFromOwner asks user which owner's pets they want to see.
     * It also calls a method which returns an array of pets and another
     * method which orders and prints out pets based on date registered.
     */
    private static void PrintPetsFromOwner()
    {
        if(owners.size() != 0)
        {
            PrintAllOwners();
            System.out.println("Enter owner ID to show their pets");
            String ID = choiceInputString().toUpperCase();

            int index = CheckOwnerID(owners, ID);
            if(index >= 0)
            {
                ArrayList<Pet> ownerPets = GetPetsFromOwner(index);
                if(ownerPets.size() != 0)
                {
                    OrderAndShowPetsByDate(ownerPets);
                }
                else
                {
                    System.out.println("Owner has no assigned pets");
                }
            }
            else
            {
                System.out.println("Owner ID not found");
            }
        }
        else
        {
            System.out.println("There are no owners registered");
        }
        OutputAvailableOptions();
    }

    /*
     * GetPetsFromOwner adds pets into a pet arrayList once
     * petOwnerID and OwnerID are the same(meaning pet is assigned to that owner)
     * and returns it
     *
     * @param ownerIndex - index used to find owner to compare petOwnerId to
     */
    private static ArrayList<Pet> GetPetsFromOwner(int ownerIndex)
    {
        ArrayList<Pet> ownerPets = new ArrayList<>();
        for(Mammal mammal: mammals)
        {
            if(mammal.getOwnerID().equals(owners.get(ownerIndex).getOwnerID()))
            {
                ownerPets.add(mammal);
            }
        }
        for(Bird bird: birds)
        {
            if(bird.getOwnerID().equals(owners.get(ownerIndex).getOwnerID()))
            {
                ownerPets.add(bird);
            }
        }
        for(Fish petFish: fish)
        {
            if(petFish.getOwnerID().equals(owners.get(ownerIndex).getOwnerID()))
            {
                ownerPets.add(petFish);
            }
        }
        return ownerPets;
    }

    /*
     * OrderAndShowPetsByDate sorts an arrayList based on date registered(descending)
     * and prints out all pets in the array
     */
    private static void OrderAndShowPetsByDate(ArrayList<Pet> ownerPets)
    {
        Collections.sort(ownerPets);

        for(int i = 0; i < ownerPets.size(); i++)
        {
            if(ownerPets.get(i).getAnimalType().toLowerCase().equals("mammal"))
            {
                System.out.println(ownerPets.get(i).toString());
            }
            else if(ownerPets.get(i).getAnimalType().toLowerCase().equals("bird"))
            {
                System.out.println(ownerPets.get(i).toString());
            }
            else
            {
                System.out.println(ownerPets.get(i).toString());
            }
        }
    }

    /*
     * ReadInPetsFromFile runs at the beginning of the program.
     * It reads in pet details in petDetails.txt and creates
     * instances of pets. It then calls a method to
     * overwrite the file in case there were duplicates.
     */
    private static void ReadInPetsFromFile()
    {
        try(Scanner scanner = new Scanner(new BufferedReader(new FileReader("petDetails.txt"))))
        {
            while(scanner.hasNextLine())
            {
                String input = scanner.nextLine();
                String[] data = input.split(",");
                String dateRegistered = data[0];
                String petID = data[1];
                String ownerID = data[2];
                String animalType = data[3];
                String type = data[4];
                String name = data[5];
                String breed = data[6];
                String age = data[7];
                String colour = data[8];
                String gender = data[9];
                if(animalType.equals("mammal"))
                {
                    String neutered = data[10];
                    Mammal temp = new Mammal(dateRegistered, petID, ownerID, animalType, type, name, breed, Integer.parseInt(age), colour, gender, Boolean.parseBoolean(neutered));
                    AddPetToSystem(temp);
                }
                else if(animalType.equals("bird"))
                {
                    String wingspan = data[10];
                    String canFly = data[11];
                    Bird temp = new Bird(dateRegistered, petID, ownerID, animalType, type, name, breed, Integer.parseInt(age), colour, gender, Double.parseDouble(wingspan), Boolean.parseBoolean(canFly));
                    AddPetToSystem(temp);
                }
                else
                {
                    String waterType = data[10];
                    Fish temp = new Fish(dateRegistered, petID, ownerID, animalType, type, name, breed, Integer.parseInt(age), colour, gender, waterType);
                    AddPetToSystem(temp);
                }
            }
            WritePetDetailsToFile();
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
    }

    /*
     * AddPetToSystem checks if the pets from the file are duplicates and only adds pets
     * to the corresponding arraylist of petExists = false
     *
     * @param pet - method takes in a pet which is compared along existing arraylist of
     * corresponding pet types to see if its a duplicate
     */
    private static void AddPetToSystem(Pet pet)
    {
        boolean petExists = false;
        if(pet.getAnimalType().equals("mammal"))
        {
            if(mammals.size() == 0)
            {
                mammals.add((Mammal)(pet));
            }
            else
            {
                for(int i = 0; i < mammals.size(); i++)
                {
                    petExists = mammals.get(i).comparePets((Mammal)(pet));
                    if(petExists)
                    {
                        break;
                    }
                }
                if(!petExists)
                {
                    mammals.add((Mammal)(pet));
                }
            }
        }
        else if(pet.getAnimalType().equals("bird"))
        {
            if(birds.size() == 0)
            {
                birds.add((Bird)(pet));
            }
            else
            {
                for(int i = 0; i < birds.size(); i++)
                {
                    petExists = birds.get(i).comparePets((Bird)(pet));
                    if(petExists)
                    {
                        break;
                    }
                }
                if(!petExists)
                {
                    birds.add((Bird)(pet));
                }
            }
        }
        else
        {
            if(fish.size() == 0)
            {
                fish.add((Fish)(pet));
            }
            else
            {
                for(int i = 0; i < fish.size(); i++)
                {
                    petExists = fish.get(i).comparePets((Fish)(pet));
                    if(petExists)
                    {
                        break;
                    }
                }
                if(!petExists)
                {
                    fish.add((Fish)(pet));
                }
            }
        }
    }

    private static void ReadInOwnersFromFile()
    {
        try(Scanner scanner = new Scanner(new BufferedReader(new FileReader("ownerDetails.txt"))))
        {
            while(scanner.hasNextLine())
            {
                String input = scanner.nextLine();
                String[] data = input.split(",");
                String ID = data[0];
                String name = data[1];
                String email = data[2];
                String address = data[3];
                String telephone = data[4];

                Owner temp = new Owner(name, email, telephone, address, ID);
                AddOwnerToSystem(temp);
            }
            WriteOwnerDetailsToFile();
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
    }

    private static void AddOwnerToSystem(Owner temp)
    {
        boolean ownerExists = false;
        if(owners.size() == 0)
        {
            owners.add(temp);
        }
        else
        {
            for(Owner owner : owners)
            {
                ownerExists = owner.compareOwners(temp);
                if(ownerExists)
                {
                    break;
                }
            }
            if(!ownerExists)
            {
                owners.add(temp);
            }
        }
    }

    /*
     * PetAgeAvg gets the age of all pets and finds average.
     * The average is then printed out
     */
    private static void PetAgeAvg()
    {
        int age = 0;
        for(Mammal mammal: mammals)
        {
            age += mammal.getAge();
        }
        for(Bird bird: birds)
        {
            age += bird.getAge();
        }
        for(Fish petFish: fish)
        {
            age += petFish.getAge();
        }
        double avg = (double)age/TotalPets();
        System.out.println("Average age of all pets: " + String.format("%.1f", avg));
    }

    /*
     * PetPercent prints out the percentage of pets
     * registered of a certain type
     */
    private static void PetPercent()
    {
        int totalPets = TotalPets();
        double noOfMammals = (double)mammals.size() * 100/totalPets;
        double noOfBirds = (double)birds.size() * 100/totalPets;
        double noOfFish = (double)fish.size() * 100/totalPets;

        System.out.println("Percent of mammals registered: " + String.format("%.1f", noOfMammals));
        System.out.println("Percent of birds registered: " + String.format("%.1f", noOfBirds));
        System.out.println("Percent of fish registered: " + String.format("%.1f", noOfFish));
    }

    /*
     * TotalPets adds the size of all arrays and returns total
     */
    private static int TotalPets()
    {
        return mammals.size()+birds.size()+fish.size();
    }

}
